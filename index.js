/*
3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
a. Name (String)
b. Age (Number)
c. Pokemon (Array)
d. Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message 
Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
8. Create a git repository named S23.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.
*/

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends:{hoenn:["May", "Max"], kanto:["Brock","Misty"]},
	talk: function(){
				console.log("Pikachu! I choose you!");
			}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();

/*
2.Create objects out of a Pokemon constructor
*/
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = level*2;
	this.attack = level;

	// Method
	// "target" parameter represents another pokemon object
	this.tackle = function(target){
		console.log(this.name + 'tackled ' + target.name);
		target.health -= this.attack;
		console.log("target Pokemon's health is now reduced to " + target.health)
	}
	this.faint = function(){
		console.log(this.name + "fainted");
	}
}

let pikachu = new Pokemon("Pikachu ", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude ", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo ", 100);
console.log(mewtwo);

/*
3.Run each Pokemon's object methods
*/
geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
geodude.faint();
console.log(geodude);
